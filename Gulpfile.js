'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');





  gulp.task('sass', gulp.series(()=>{
    return gulp.src(['./css/*.scss'])
           .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./css'))             
            .pipe(browserSync.stream()); 
  }));
    
gulp.task('serve', gulp.series('sass', function(){ 
    browserSync.init({ 
      server: './'
    });
    gulp.watch([ 
      './css/*.scss',
      ], gulp.series('sass')
    );
   gulp.watch('./*.html').on('change', browserSync.reload); 
  }));

gulp.task('default', gulp.parallel('serve'));

  
gulp.task('clean', function () {
 return del(['dist']);


});

 gulp.task('copyfonts', ()=>{
    return gulp.src('node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot, otf}*')
            .pipe(gulp.dest('./dist/fonts'));             
  });

  gulp.task('imagemin', ()=> {
    return gulp.src(['./images/*.{png,jpg,jpeg,gif}'])
            .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
            .pipe(gulp.dest('dist/images'))       
  });

  gulp.task('usemin', ()=> {
    return gulp.src('./*.html')
           .pipe(flatmap((stream, file)=>{
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function(){ return htmlmin({collapseWhitespace: true})}],
                    js:[uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']  
                }));
           }))
             .pipe(gulp.dest('dist'));        
  });
  

  

  gulp.task('build',gulp.series('clean',gulp.parallel('copyfonts','imagemin','usemin')))
 
